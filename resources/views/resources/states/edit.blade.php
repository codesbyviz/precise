@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h3>Edit {{$state->name}}</h3>
        <a href="{{route('state.show',[$state->id])}}" class="float-right btn btn-danger">Cancel</a>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('state.update',[$state->id])}}" method="post">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group">
                                <label for="precise_state_name">Name</label>
                                <input type="text" name="name" id="precise_state_name" class="form-control" value="@if(old('name')) {{old('name')}} @else {{$state->name}}  @endif">
                            </div>
                            <div class="form-group">
                                <label for="precise_status">Status</label>
                                <select name="status" id="precise_status" class="form-control">
                                    <option @if(old('status') == '1' || $state->status == '1') selected @endif value="1">Active</option>
                                    <option @if(old('status') == '0' || $state->status == '0') selected @endif value="0">InActive</option>
                                </select>
                            </div>
                            <div class="form-group">
                                    <label for="precise_country">Status</label>
                                    <select name="country" id="precise_country" class="form-control" required>
                                            <option selected disabled value="0">-- Select Country --</option>
                                            @forelse ($countries as $country)
                                            <option @if(old('state') == $country->id || $state->country == $country->id ) selected @endif value="{{$country->id}}">{{$country->name}}</option>
                                        @empty
                                            <option selected disabled value="0">--No Countries Available --</option>
                                        @endforelse
                                    </select>
                                </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection