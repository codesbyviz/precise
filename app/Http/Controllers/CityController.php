<?php

namespace App\Http\Controllers;

use App\City;
use App\State;
use App\Country;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::paginate(10);
        return view("resources.cities.index",['cities'=>$cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view("resources.cities.create",["countries"=>$countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"=>"required",
            "state"=>"int|required",
            "status"=>"int",
        ]);
        $city = new City;
        $city->name = $request->name;
        $city->status = $request->status;
        $city->state = $request->state;
        $city->save();
        return redirect()->route('city.show',[$city->id])->with(['status'=>"New City for $city->State Created"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        return view("resources.cities.show",["city"=>$city]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        $countries = Country::all();
        return view("resources.cities.edit",["countries"=>$countries,"city"=>$city]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $request->validate([
            "name"=>"required",
            "state"=>"int|required",
            "status"=>"int",
        ]);
        $city->name = $request->name;
        $city->status = $request->status;
        $city->state = $request->state;
        $city->save();
        return redirect()->route('city.show',[$city->id])->with(['status'=>"$city->name Updated"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        $city->delete();
        return redirect()->route('city.index')->with(['status'=>"Deleted"]);
    }
}
