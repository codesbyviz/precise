@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h3>Create new Country</h3>
        <a href="{{route('country.index')}}" class="float-right btn btn-danger">Back</a>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('country.store')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="precise_country_name">Name</label>
                                <input type="text" name="name" id="precise_country_name" class="form-control" value="{{old('name')}}">
                            </div>
                            <div class="form-group">
                                <label for="precise_status">Status</label>
                                <select name="status" id="precise_status" class="form-control">
                                    <option @if(old('status') == '1') selected @endif value="1">Active</option>
                                    <option @if(old('status') == '0') selected @endif value="0">InActive</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection