@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h3>{{$state->name}}</h3>
        {!!$state->Status()!!}
        <div class="float-right btn-group">
            <a href="{{route('state.index')}}" class="btn btn-sm btn-danger">Back</a>
            <a href="{{route('state.edit',[$state->id])}}" class="btn btn-sm btn-outline-info">Edit</a>
            <form action="{{route('state.destroy',[$state->id])}}" method="post">
                @csrf
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <table class="table table-sm">
                    <thead class="table-dark">
                        <tr>
                            <th>city</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                       @forelse ($state->Cities as $city)
                           <tr>
                               <th><a href="{{route('city.show',[$city->id])}}">{{$city->name}}</a></th>
                               <th>{!!$city->Status()!!}</th>
                           </tr>
                       @empty
                           <th class="text-center" colspan="2">
                               No cities Listed
                           </th>
                       @endforelse
                    </tbody>
                </table>                
            </div>
        </div>
    </div>
@endsection