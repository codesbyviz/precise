<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public function Status()
    {
        switch ($this->status) {
            case '1':
                $status = "<span class=\"badge badge-success\">Active</span>";
                break;
            case '0':
                $status = "<span class=\"badge badge-danger\">InActive</span>";
                break;
            default:
                $status = "<span class=\"badge badge-warning\">Unknown</span>";
                break;
        }
        return $status;
    }

    public function Country()
    {
        return $this->belongsTo('App\Country', 'country', 'id');
    }
    public function Cities()
    {
        return $this->hasMany('App\City', 'state', 'id');
    }

}
