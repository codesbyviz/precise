@extends('layouts.app')
@section('content')
<div class="jumbotron">
    <h3>{{$country->name}}</h3>
    {!!$country->Status()!!}
    <div class="float-right btn-group">
        <a href="{{route('country.index')}}" class="btn btn-sm btn-danger">Back</a>
        <a href="{{route('country.edit',[$country->id])}}" class="btn btn-sm btn-outline-info">Edit</a>
        <form action="{{route('country.destroy',[$country->id])}}" method="post">
            @csrf
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
        </form>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-4">
            <div class="card">
                <table class="table table-sm">
                    <thead class="table-dark">
                        <tr>
                            <th>State</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                       @forelse ($country->States as $state)
                           <tr>
                               <th><a href="{{route('state.show',[$state->id])}}">{{$state->name}}</a></th>
                               <th>{!!$state->Status()!!}</th>
                           </tr>
                       @empty
                           <th class="text-center" colspan="2">
                               No states Listed
                           </th>
                       @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection