@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h3>Cities</h3>
        <a href="{{route('city.create')}}" class="float-right btn btn-info">Create</a>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-6">
                <div class="card">
                    <table class="table table-sm">
                        <thead class="table-dark">
                            <tr>
                                <th>city</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                           @forelse ($cities as $city)
                               <tr>
                                   <th><a href="{{route('city.show',[$city->id])}}">{{$city->name}}</a></th>
                                   <th>{!!$city->Status()!!}</th>
                               </tr>
                           @empty
                               <th class="text-center" colspan="2">
                                   No cities Listed
                               </th>
                           @endforelse
                        </tbody>
                    </table>
                    <div class="card-footer">
                        {{$cities->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection