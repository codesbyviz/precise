@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h3>Edit {{$city->name}}</h3>
        <a href="{{route('city.show',[$city->id])}}" class="float-right btn btn-danger">Cancel</a>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('city.update',[$city->id])}}" method="post">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group">
                                <label for="precise_city_name">Name</label>
                                <input type="text" name="name" id="precise_city_name" class="form-control" value="@if(old('name')) {{old('name')}} @else {{$city->name}}  @endif">
                            </div>
                            <div class="form-group">
                                <label for="precise_status">Status</label>
                                <select name="status" id="precise_status" class="form-control">
                                    <option @if(old('status') == '1' || $city->status == '1') selected @endif value="1">Active</option>
                                    <option @if(old('status') == '0' || $city->status == '0') selected @endif value="0">InActive</option>
                                </select>
                            </div>
                            <div class="form-group">
                                    <label for="precise_state">State</label>
                                    <select name="state" id="precise_state" class="form-control" required>
                                            <option selected disabled value="0">-- Select state --</option>
                                            @forelse ($countries as $country)
                                            <optgroup label="{{$country->name}}">
                                                @forelse ($country->States as $state)
                                                    <option @if(old('state') == $state->id || $city->state == $state->id) selected @endif value="{{$state->id}}">{{$state->name}}</option>
                                                @empty
                                                @endforelse
                                            </optgroup>
                                            @empty
                                            <option selected disabled value="0">--No states Available --</option>
                                        @endforelse
                                    </select>
                                </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection